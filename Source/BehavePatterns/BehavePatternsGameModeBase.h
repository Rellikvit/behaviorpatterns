// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BehavePatternsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVEPATTERNS_API ABehavePatternsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
