// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerCharacter.h"
#include "GameFramework/Actor.h"
#include "SpectatorActor.generated.h"

UCLASS()
class BEHAVEPATTERNS_API ASpectatorActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpectatorActor();

	UFUNCTION()
	void RePrintString(FString& StringToReprint);

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category=TargetActor)
	APlayerCharacter* TargetPlayer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
