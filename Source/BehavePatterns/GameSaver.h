// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SaveActor.h"
#include "GameFramework/SaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "PlayerCharacter.h"
#include "GameSaver.generated.h"

/**
 * 
 */

class APlayerCharacter;

UCLASS()
class UGameSaver : public USaveGame
{
	GENERATED_BODY()
public:
	
	static UGameSaver* GetInstance();
	
	void SavePlayer(APlayerCharacter* ActorToSave);
	
	void SaveGame();
	void LoadGame();
	
	FString SavedPlayerString;

private:
	static UGameSaver* Instance;
};

inline UGameSaver* UGameSaver::GetInstance()
{
	if(!Instance)
	{
		Instance = Cast<UGameSaver>(UGameplayStatics::CreateSaveGameObject(StaticClass()));
	}
	return Instance;
}

inline void UGameSaver::SavePlayer(APlayerCharacter* ActorToSave)
{
	SavedPlayerString = ActorToSave->PrevString;
}

inline void UGameSaver::SaveGame()
{
	const auto Saver = GetInstance();
	if(Saver)
	{
		for(FActorIterator Iter(Saver->GetWorld()); Iter; ++Iter)
		{
			const ISaveActor* TheInterfaceActor = Cast<ISaveActor>(*Iter);
			TheInterfaceActor->Execute_SaveData(TheInterfaceActor->_getUObject(),Saver);
		}
		UGameplayStatics::AsyncSaveGameToSlot(Saver,"Save",0);
	}
}

inline void UGameSaver::LoadGame()
{
	Instance = Cast<UGameSaver>(UGameplayStatics::LoadGameFromSlot("Save",0));
	const auto Saver = GetInstance();
	if(Saver)
	{
		for(FActorIterator Iter(Saver->GetWorld()); Iter; ++Iter)
		{
			const ISaveActor* TheInterfaceActor = Cast<ISaveActor>(*Iter);
			TheInterfaceActor->Execute_LoadData(TheInterfaceActor->_getUObject(),Instance);
		}
	}
}

