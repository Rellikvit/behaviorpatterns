// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SaveActor.h"
#include "Step.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStringRePrint, FString&, StringToReprint);

UENUM(BlueprintType)
enum class EMoveState : uint8
{
	Idle = 0,
	Walking = 1
};

UCLASS()
class BEHAVEPATTERNS_API APlayerCharacter : public ACharacter, public ISaveActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//SaveInterface
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
		void SaveData(UGameSaver* Saver);
	void SaveData_Implementation(UGameSaver* Saver) override;
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
		void LoadData(UGameSaver* Saver);
	void LoadData_Implementation(UGameSaver* Saver) override;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = Movement)
		EMoveState MovementState;

	FString PrevString = "";

	FOnStringRePrint StringRePrint;
	
private:

    FString CharacterString = "";

	int32 StringLength = 0;

	void AddQ();
	void AddE();
	void AddR();

	void UndoCommand();

	void PrintString();
	
	TArray<UStep*> Steps;
};
