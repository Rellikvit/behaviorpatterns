// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Step.generated.h"

UCLASS()
class BEHAVEPATTERNS_API UStep : public UObject
{
	GENERATED_BODY()
public:
	
	virtual void Execute(FString& FinalString);
};

UCLASS()
class BEHAVEPATTERNS_API UQStep : public UStep
{
	GENERATED_BODY()
	virtual void Execute(FString& FinalString) override;
};
UCLASS()
class BEHAVEPATTERNS_API UEStep : public UStep
{
	GENERATED_BODY()
	virtual void Execute(FString& FinalString) override;
};
UCLASS()
class BEHAVEPATTERNS_API URStep : public UStep
{
	GENERATED_BODY()
	virtual void Execute(FString& FinalString) override;
};