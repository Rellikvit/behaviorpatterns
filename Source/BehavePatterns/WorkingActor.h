// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WorkingActor.generated.h"

UCLASS()
class BEHAVEPATTERNS_API AWorkingActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorkingActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void StartDoingJob();

	virtual void FoundWay();

	virtual void GatherResources();

	virtual void Repeat();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
