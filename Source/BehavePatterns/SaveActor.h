#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SaveActor.generated.h"

class UGameSaver;

UINTERFACE(Blueprintable)
class USaveActor : public UInterface
{
	
public:
	GENERATED_BODY()
};

class ISaveActor
{
	
public:
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	void SaveData(UGameSaver* Saver);
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	void LoadData(UGameSaver* Saver);
};
