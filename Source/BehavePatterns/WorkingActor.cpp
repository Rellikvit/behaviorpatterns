// Fill out your copyright notice in the Description page of Project Settings.


#include "WorkingActor.h"

// Sets default values
AWorkingActor::AWorkingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AWorkingActor::BeginPlay()
{
	Super::BeginPlay();
	StartDoingJob();
}

void AWorkingActor::StartDoingJob()
{
	FoundWay();
	GatherResources();
}

void AWorkingActor::FoundWay()
{
	GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::Purple, FString::Printf(TEXT("FoundingWay")));
}

void AWorkingActor::GatherResources()
{
	GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::Purple, FString::Printf(TEXT("GatheringResourses")));
}

void AWorkingActor::Repeat()
{
	//RepeatPrevoisSteps
}

// Called every frame
void AWorkingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

