// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WorkingActor.h"
#include "MiningActor.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVEPATTERNS_API AMiningActor : public AWorkingActor
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GatherResources() override;
	
};
