// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "GameSaver.h"
#include "SpectatorActor.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("PressQ", IE_Pressed, this, &APlayerCharacter::AddQ);
	InputComponent->BindAction("PressE", IE_Pressed, this, &APlayerCharacter::AddE);
	InputComponent->BindAction("PressR", IE_Pressed, this, &APlayerCharacter::AddR);

	InputComponent->BindAction("UndoCommand", IE_Pressed, this, &APlayerCharacter::UndoCommand);

	InputComponent->BindAction("PrintString", IE_Pressed, this, &APlayerCharacter::PrintString);
}

void APlayerCharacter::SaveData_Implementation(UGameSaver* Saver)
{
	if(Saver)Saver->SavePlayer(this);
}

void APlayerCharacter::LoadData_Implementation(UGameSaver* Saver)
{
	if(Saver)
	{
		PrevString = Saver->SavedPlayerString;
		CharacterString = Saver->SavedPlayerString;
	}
}

void APlayerCharacter::AddQ()
{
	if(StringLength < 10)
	{
		Steps.Add(NewObject<UQStep>());
		StringLength++;
	}
}

void APlayerCharacter::AddE()
{
	if(StringLength < 10)
	{
		Steps.Add(NewObject<UEStep>());
		StringLength++;
	}
}

void APlayerCharacter::AddR()
{
	if(StringLength < 10)
	{
		Steps.Add(NewObject<URStep>());
		StringLength++;
	}
}

void APlayerCharacter::UndoCommand()
{
	if(StringLength > 0)
	{
		StringLength--;
		Steps.Pop();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("String Empty")));
	}
}

void APlayerCharacter::PrintString()
{
	for(int i = 0; i < Steps.Num();i++)
	{
		Steps[i]->Execute(CharacterString);
	}
	GEngine->AddOnScreenDebugMessage(-1, 6.f, FColor::White, FString::Printf(TEXT("OutputString: %s"), ToCStr(CharacterString)));
	StringRePrint.Broadcast(CharacterString);
	PrevString = CharacterString;
	CharacterString.RemoveFromStart(CharacterString);
}


