// Fill out your copyright notice in the Description page of Project Settings.


#include "SpectatorActor.h"
#include "PlayerCharacter.h"

// Sets default values
ASpectatorActor::ASpectatorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASpectatorActor::BeginPlay()
{
	Super::BeginPlay();
	if(TargetPlayer) TargetPlayer->StringRePrint.AddDynamic(this,&ASpectatorActor::RePrintString);
}

void ASpectatorActor::RePrintString(FString& StringToReprint)
{
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("SpectatorString: %s"), ToCStr(StringToReprint)));
}

// Called every frame
void ASpectatorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

